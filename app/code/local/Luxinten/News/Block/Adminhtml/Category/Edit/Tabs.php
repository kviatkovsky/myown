<?php

class Luxinten_News_Block_Adminhtml_Category_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('category_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle($this->__('Category Information'));
    }

    protected function _prepareLayout()
    {
        $this->addTab('general_section', array(
            'label' => $this->__('General Information'),
            'title' => $this->__('General Information'),
            'content' => $this->getLayout()->createBlock('luxinten_news/adminhtml_category_edit_tabs_general')->toHtml(),
        ));
        $category = Mage::registry('current_category');
        if($category->getId()) {
            $this->addTab('news_section', array(
                'class' => 'ajax',
                'label' => $this->__('News'),
                'title' => $this->__('News'),
                'url' => $this->getUrl('*/*/news', array('_current' => true)),
            ));
        }

        return parent::_prepareLayout();
    }

}

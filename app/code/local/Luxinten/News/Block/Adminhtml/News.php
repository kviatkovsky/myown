<?php

class Luxinten_News_Block_Adminhtml_News extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    protected function _construct()
    {
        parent::_construct();

        $this->_blockGroup = 'luxinten_news';
        $this->_controller = 'adminhtml_news';

        $this->_headerText = $this->__('News Management');
        $this->_addButtonLabel = $this->__('Add News');
    }

}

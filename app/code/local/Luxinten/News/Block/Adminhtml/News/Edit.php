<?php

class Luxinten_News_Block_Adminhtml_News_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{

    protected function _construct()
    {
        $this->_blockGroup = 'luxinten_news';
        $this->_controller = 'adminhtml_news';
    }

    public function getHeaderText()
    {
        $model = Mage::registry('current_news');

        if ($model->getId()) {
            return $this->__("Edit News item '%s'", $this->escapeHtml($model->getTitle()));
        } else {
            return $this->__("Add News item");
        }
    }

}

<?php

class Luxinten_News_Block_Adminhtml_News_Edit_Tabs_General extends Mage_Adminhtml_Block_Widget_Form
{

    protected function _prepareForm()
    {
        $model = Mage::registry('current_news');

        $form = new Varien_Data_Form();
        $fieldset = $form->addFieldset('general_form', array(
            'legend' => $this->__('General Information')
        ));

        $fieldset->addField('title', 'text', array(
            'label' => $this->__('Title'),
            'required' => true,
            'name' => 'title',
        ));

        $fieldset->addField('category_id', 'select', array(
            'label' => $this->__('Category'),
            'name' => 'category_id',
            'values' => Mage::helper('luxinten_news')->getCategoriesOptions(),
        ));

        $fieldset->addField('content', 'editor', array(
            'label' => $this->__('Content'),
            'required' => true,
            'name' => 'content',
        ));

        $fieldset->addField('image', 'image', array(
            'label' => $this->__('Image'),
            'name' => 'image',
        ));

        $fieldset->addField('created', 'date', array(
            'format' => Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT),
            'image' => $this->getSkinUrl('images/grid-cal.gif'),
            'label' => $this->__('Created'),
            'name' => 'created'
        ));

        $fieldset->addField('link', 'text', array(
            'label' => $this->__('Link'),
            'name' => 'link',
        ));

        $formData = array_merge($model->getData(), array('image' => $model->getImageUrl()));
        $form->setValues($formData);
        $this->setForm($form);

        return parent::_prepareForm();
    }

}

<?php

class Luxinten_News_Model_Category extends Mage_Core_Model_Abstract
{

    protected function _construct()
    {
        parent::_construct();
        $this->_init('luxinten_news/category');
    }

    protected function _afterDelete()
    {
        $newsCollection = Mage::getModel('luxinten_news/news')->getCollection()
            ->addFieldToFilter('category_id', $this->getId());
        foreach($newsCollection as $news){
            $news->setCategoryId(0)->save();
        }
        return parent::_afterDelete();
    }

    public function getNewsCollection()
    {
        $collection = Mage::getModel('luxinten_news/news')->getCollection();
        $collection->addFieldToFilter('category_id', $this->getId());
        return $collection;
    }


}

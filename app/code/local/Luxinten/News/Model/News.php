<?php

class Luxinten_News_Model_News extends Mage_Core_Model_Abstract
{

    public function _construct()
    {
        $this->_init('luxinten_news/news');
    }

    protected function _afterDelete()
    {
        $helper = Mage::helper('luxinten_news');
        @unlink($helper->getImagePath($this->getId()));
        return parent::_afterDelete();
    }

    protected function _beforeSave()
    {
        $helper = Mage::helper('luxinten_news');

        if (!$this->getData('link')) {
            $this->setData('link', $helper->prepareUrl($this->getTitle()));
        } else {
            $this->setData('link', $helper->prepareUrl($this->getData('link')));
        }
        return parent::_beforeSave();
    }

    public function getImageUrl()
    {
        $helper = Mage::helper('luxinten_news');
        if ($this->getId() && file_exists($helper->getImagePath($this->getId()))) {
            return $helper->getImageUrl($this->getId());
        }
        return null;
    }

}

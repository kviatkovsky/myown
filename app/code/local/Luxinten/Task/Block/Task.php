<?php

class Luxinten_Task_Block_Task extends Mage_Core_Block_Template
{

    public function getNewsCollection()
    {
        $newsCollection = Mage::getModel('luxinten_task/task')->getCollection();
        $newsCollection->setOrder('created', 'DESC');
        return $newsCollection;
    }

}

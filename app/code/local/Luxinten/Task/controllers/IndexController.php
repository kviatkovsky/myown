<?php

class Luxinten_Task_IndexController extends Mage_Core_Controller_Front_Action
{

    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function viewAction()
    {
        $taskId = Mage::app()->getRequest()->getParam('id', 0);
        $task = Mage::getModel('luxinten_task/task')->load($taskId);

        if ($task->getId() > 0) {
            $this->loadLayout();
            $this->getLayout()->getBlock('task.content')->assign(array(
                "taskItem" => $task,
            ));
            $this->renderLayout();
        } else {
            $this->_forward('noRoute');
        }
    }



}